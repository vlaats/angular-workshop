import { Component, Input } from '@angular/core';
import { TodoInterface } from '../../types/todo.interface';
import { TodosService } from '../../services/todos.service';

@Component({
  selector: 'app-todos-todo',
  templateUrl: './todo.component.html',
})
export class TodoComponent {
  @Input('todo') todoProps?: TodoInterface;

  constructor(private todosService: TodosService) {}

  removeTodo(): void {
    if (this.todoProps?.id) {
      this.todosService.removeTodo(this.todoProps.id);
    }
  }

  toggleTodo(): void {
    if (this.todoProps?.id) {
      this.todosService.toggleTodo(this.todoProps);
    }
  }
}
