import { Component, OnInit } from '@angular/core';
import { combineLatestWith, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TodoInterface } from '../../types/todo.interface';
import { TodosService } from '../../services/todos.service';
import { FilterEnum } from '../../types/filter.enum';

@Component({
  selector: 'app-todos-main',
  templateUrl: './main.component.html',
})
export class MainComponents implements OnInit {
  visibleTodos$: Observable<TodoInterface[]>;
  noTodos$: Observable<boolean>;
  isAllTodosSelected$: Observable<boolean>;

  constructor(private todosService: TodosService) {
    this.noTodos$ = this.todosService.todos$.pipe(
      map((todos) => todos.length === 0)
    );
    this.isAllTodosSelected$ = this.todosService.todos$.pipe(
      map((todos) => todos.every((todo) => todo.isCompleted))
    );
    this.visibleTodos$ = this.todosService.todos$.pipe(
      combineLatestWith(this.todosService.filter$),
      map(([todos, filter]: [TodoInterface[], FilterEnum]) => {
        if (filter === FilterEnum.ACTIVE) {
          return todos.filter((todo) => !todo.isCompleted);
        } else if (filter === FilterEnum.COMPLETED) {
          return todos.filter((todo) => todo.isCompleted);
        }
        return todos;
      })
    );
  }

  ngOnInit(): void {
    this.todosService.getAllTodos();
  }

  toggleAllTodos(event: Event): void {
    const target = event.target as HTMLInputElement;
    this.todosService.toggleAll(target.checked);
  }
}
