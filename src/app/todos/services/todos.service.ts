import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TodoInterface } from '../types/todo.interface';
import { FilterEnum } from '../types/filter.enum';

@Injectable()
export class TodosService {
  todos$ = new BehaviorSubject<TodoInterface[]>([]);
  filter$ = new BehaviorSubject<FilterEnum>(FilterEnum.ALL);

  constructor(private httpClient: HttpClient) {}

  getAllTodos(): void {
    this.httpClient
      .get<TodoInterface[]>(`${environment.apiBaseUrl}/todos`)
      .subscribe((data) => {
        this.todos$.next(data);
      });
  }

  addTodo(text: string): void {
    const newTodo: TodoInterface = {
      id: Math.random().toString(16),
      isCompleted: false,
      text,
    };
    const updatedTodos = [...this.todos$.getValue(), newTodo];
    this.httpClient
      .post<TodoInterface>(`${environment.apiBaseUrl}/todos`, newTodo)
      .subscribe(() => {
        this.todos$.next(updatedTodos);
      });
  }

  toggleAll(isCompleted: boolean): void {
    const updatedTodos = this.todos$.getValue().map((todo) => {
      return {
        ...todo,
        isCompleted,
      };
    });
    this.todos$.next(updatedTodos);
  }

  changeFilter(filterName: FilterEnum): void {
    this.filter$.next(filterName);
  }

  removeTodo(todoId: string): void {
    const updatedTodos = this.todos$
      .getValue()
      .filter((todo) => todo.id !== todoId);

    this.httpClient
      .delete<TodoInterface>(`${environment.apiBaseUrl}/todos/${todoId}`)
      .subscribe(() => {
        this.todos$.next(updatedTodos);
      });
  }

  getTodoById(todoId: string): TodoInterface | undefined {
    return this.todos$
      .getValue()
      .filter((todo) => todo.id === todoId)
      .pop();
  }

  updateTodoById(todoId: string, values: Object = {}): TodoInterface | null {
    const todo = this.getTodoById(todoId);
    if (!todo) {
      return null;
    }
    Object.assign(todo, values);
    return todo;
  }

  toggleTodo(selectedTodo: TodoInterface): void {
    const updatedTodo = this.updateTodoById(selectedTodo.id, {
      isCompleted: !selectedTodo?.isCompleted,
    });
    if (updatedTodo) {
      const updatedTodos = this.todos$.getValue().map((todo) => {
        if (todo.id === updatedTodo.id) {
          return updatedTodo;
        }
        return todo;
      });
      this.httpClient
        .put<TodoInterface>(
          `${environment.apiBaseUrl}/todos/${selectedTodo.id}`,
          updatedTodo
        )
        .subscribe(() => {
          this.todos$.next(updatedTodos);
        });
    }
  }
}
