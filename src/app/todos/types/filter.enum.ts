export enum FilterEnum {
  ACTIVE = 'ACTIVE',
  ALL = 'ALL',
  COMPLETED = 'COMPLETED',
}
